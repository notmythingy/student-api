package com.example.studentapi.student;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalDate;
import java.time.Month;
import java.util.List;

@Configuration
public class StudentConfig {

    @Bean
    CommandLineRunner commandLineRunner(
            StudentRepository repository
    ) {
        return args -> {
            Student mirkku = new Student(
                    "Mirkku",
                    "mirkka@mirkku&markku.org",
                    LocalDate.of(1986, Month.APRIL, 8)
            );
            Student markku = new Student(
                    "Markku",
                    "markku@mirkku&markku.org",
                    LocalDate.of(1984, Month.FEBRUARY, 12)
            );
            repository.saveAll(List.of(mirkku, markku));
        };
    }
}
